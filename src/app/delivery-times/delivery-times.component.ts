import { Component, OnInit } from '@angular/core';

import {HomeDeliveryService} from '../services/HomeDelivery.service';
import {DeliveryTimeService} from '../services/DeliveryTime.service';
import {DeliveryTimeFormattedModel} from '../models/DeliveryTimeFormatted.model';


@Component({
  selector:     'app-delivery-times',
  templateUrl:  './delivery-times.component.html',
  providers:    [DeliveryTimeService]
})

export class DeliveryTimesComponent implements OnInit {
  items: DeliveryTimeFormattedModel[];
  disabled: boolean;
  selectedItem: number;
  homeDeliveryMode: boolean;

  constructor(
      private deliveryTimes: DeliveryTimeService,
      private homeDelivery: HomeDeliveryService) {

      this.deliveryTimes.availableTimes.subscribe({
        next: items => this.items = items,
      });

      this.homeDelivery.mode.subscribe({
        next: mode => {
            const timeSlots = this.deliveryTimes.getItems();
            const homeDeliverySlots = timeSlots.filter((item) => item.inHomeAvailable);
            const nonHomeDeliverySlotIds = timeSlots
                                            .filter((item) => !item.inHomeAvailable)
                                            .map(items => items.deliveryTimeId);
            if (mode === true && nonHomeDeliverySlotIds.indexOf(this.selectedItem) !== -1 ) {
                this.resetSelection();
            }
            this.items = mode ?  homeDeliverySlots : timeSlots;
        },
      });

    }

  onSelection(selectedId) {
    this.disabled = false;
    this.selectedItem = selectedId;
  }

  resetSelection() {
      this.disabled = true;
      this.selectedItem = null;
  }

  ngOnInit() {
    this.disabled = true;
  }

}
