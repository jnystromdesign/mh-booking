import {DeliveryTimeModel} from '../models/DeliveryTime.model';
import deliveryTimes from '../../data/delivery-times';
import {timestringToComparableTime} from '../shared/shared';

// Sort the DELIVERY_TIMES
export const DELIVERY_TIMES: DeliveryTimeModel[] = deliveryTimes.data[0].deliveryTimes.sort((a, b) => {
    return timestringToComparableTime(a.startTime) - timestringToComparableTime(b.startTime);
});
