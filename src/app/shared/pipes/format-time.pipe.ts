import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'formatTime'})

export class FormatTime implements PipeTransform {
  transform(timeString: string): string {
    return timeString.replace(/:00$/, '');
  }
}
