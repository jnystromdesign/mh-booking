export const timestringToComparableTime = (timeString: string): number => {
    return parseInt(timeString.replace(/:00$/, '').replace(':', ''), 10);
};

export const formatTime = (timeString: string): string => {
    return timeString.replace(/:00$/, '');
}
