import {DeliveryTimeModel} from './DeliveryTime.model'
export class ApiResponsModel {
    status: string;
    data: {
        deliveryItems: DeliveryTimeModel[]
    };
}
