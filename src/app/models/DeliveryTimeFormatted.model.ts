import {DeliveryTimeModel} from './DeliveryTime.model';
import {formatTime} from '../shared/shared';

export class DeliveryTimeFormattedModel extends DeliveryTimeModel {
    timeSlot: string;
    deliveryTimeId: number;
    startTime: string;
    stopTime: string;
    inHomeAvailable: boolean;

    constructor(item) {
        super();
        this.deliveryTimeId = parseInt(item.deliveryTimeId);
        this.startTime = item.startTime;
        this.stopTime = item.stopTime;
        this.inHomeAvailable = item.inHomeAvailable ===  'true';
        this.timeSlot = this.formatTimeSlot();
    }

    formatTimeSlot(){
        return `${formatTime(this.startTime)} - ${formatTime(this.stopTime)}`;
    }
}
