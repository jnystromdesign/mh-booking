export class DeliveryTimeModel {
    deliveryTimeId: number;
    startTime: string;
    stopTime: string;
    inHomeAvailable: boolean;
}
