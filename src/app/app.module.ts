import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeDeliveryComponent } from './home-delivery/homeDelivery.component';
import { DeliveryTimesComponent } from './delivery-times/delivery-times.component';
import {FormatTime} from './shared/pipes/format-time.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeDeliveryComponent,
    DeliveryTimesComponent,
    FormatTime
  ],

  imports: [
    BrowserModule,
    HttpClientModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
