import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class HomeDeliveryService {

    private HomeDeliveryMode = new BehaviorSubject<boolean>(false);
    mode = this.HomeDeliveryMode.asObservable();

    constructor(){

    }

    changeMode(mode: boolean){
        this.HomeDeliveryMode.next(mode);
    }
}
