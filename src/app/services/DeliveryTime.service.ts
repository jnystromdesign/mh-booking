import {Injectable, } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {DeliveryTimeFormattedModel} from '../models/DeliveryTimeFormatted.model';
import {ApiResponsModel} from '../models/ApiRespons.model';
import {timestringToComparableTime} from '../shared/shared';



@Injectable()
export class DeliveryTimeService {
    private API_URL = 'https://bitbucket.org/!api/2.0/snippets/jnystromdesign/Lex6nx/4d4f9fc1a5976aad6a82d736799fc043a176286b/files/mh-booking-data.json';
    private DeliveryTimes = new BehaviorSubject<DeliveryTimeFormattedModel[]>([]);
    availableTimes = this.DeliveryTimes.asObservable();

    constructor(private http: HttpClient) {
        this.fetchItems().subscribe({
            next: (next) => {
                const formattedItem = next.data[0].deliveryTimes.map(item => new DeliveryTimeFormattedModel(item));
                formattedItem.sort((a, b) => {
                   return timestringToComparableTime(a.startTime) - timestringToComparableTime(b.startTime);
                } );

                this.DeliveryTimes.next(formattedItem);
            },
            error: error => console.log(error)
        });
    }
    getItems(){
        return this.DeliveryTimes.getValue();
    }
    fetchItems(){
        return this.http.get<ApiResponsModel>(this.API_URL);
    }
}
