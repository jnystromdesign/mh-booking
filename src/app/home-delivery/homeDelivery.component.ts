import { Component, OnInit } from '@angular/core';
import {HomeDeliveryService} from '../services/HomeDelivery.service';

@Component({
  selector: 'app-header',
  templateUrl: './homeDelivery.component.html'
})

export class HomeDeliveryComponent implements OnInit {
  homeDeliveryMode: boolean;

  constructor(private homeDelivery: HomeDeliveryService) {

    homeDelivery.mode.subscribe({
      next: mode => this.homeDeliveryMode = mode,
    });
  }

  onCheckClick() {
    this.homeDelivery.changeMode(!this.homeDeliveryMode);
  }

  ngOnInit() {
  }

}
