# Booking

Simple example of Angular-app for completing the delivery steps. 

## What's working: 
- Sorting of data
- Toggling home delivery by state
- Enabling next step button when choosing a time. 

## What's not working: 
- Filtering of items when selecting Home delivery
